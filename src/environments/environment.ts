// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAg-Goz-P8F7JHuXVbnemtHG6U1NBxTxG0',
    authDomain: 'ccsoft-85144.firebaseapp.com',
    databaseURL: 'https://ccsoft-85144.firebaseio.com',
    projectId: 'ccsoft-85144',
    storageBucket: 'ccsoft-85144.appspot.com',
    messagingSenderId: '4114207581',
    appId: '1:4114207581:web:2092d9669ab505b20a4eb4'

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
