import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {redirectUnauthorizedTo} from '@angular/fire/auth-guard';

@Injectable()
export class Rest {

  constructor(public http: HttpClient) {
  }

  API_BASE_URL = 'http://localhost:8080/ccsoft/API/';

  insertDatosRegistro(objRegistro) {
    return this.http.post(this.API_BASE_URL + 'RegistroUsuarios/insertDatosRegistro', objRegistro);
  }

  getUserFromLogin(usuario, contrasena) {
    return this.http.get(this.API_BASE_URL + 'RegistroUsuarios/getUserFromLogin/' + usuario + '/' + contrasena);
  }

  getRegistroUsuariosPorUsername(usuario) {
    return this.http.get(this.API_BASE_URL + 'RegistroUsuarios/getRegistroUsuariosPorUsername/' + usuario);
  }

  getRegistroUsuariosPorCorreo(correo) {
    return this.http.get(this.API_BASE_URL + 'RegistroUsuarios/getRegistroUsuariosPorCorreo/' + correo);
  }

  getRegistroUsuariosPorClave(clave) {
    return this.http.get(this.API_BASE_URL + 'RegistroUsuarios/getRegistroUsuariosPorClave/' + clave);
  }

  sendEmail(objCorreo) {
    return this.http.post(this.API_BASE_URL + 'RegistroUsuarios/SendMail', objCorreo);
  }

  updateContrasena(objChangePassword) {
    return this.http.post(this.API_BASE_URL + 'RegistroUsuarios/updateContrasena', objChangePassword);
  }

  updatePerfil(objPerfil) {
    return this.http.post(this.API_BASE_URL + 'RegistroUsuarios/updatePerfil', objPerfil);
  }

  insertRelCat(objCategoria) {
    return this.http.post(this.API_BASE_URL + 'RelCatService/insertRelCat', objCategoria);
  }

  insertDenuncia(objDenuncia) {
    return this.http.post(this.API_BASE_URL + 'ReportesService/insertReportes', objDenuncia);
  }

  getReportes() {
    return this.http.get(this.API_BASE_URL + 'ReportesService/getReportes');
  }

  getReportesDenuncias(usuario) {
    return this.http.get(this.API_BASE_URL + 'ReportesService/getReportesDenuncias/' + usuario);
  }

  getReportesDenunciasAdmin() {
    return this.http.get(this.API_BASE_URL + 'ReportesService/getReportesDenunciasAdmin');
  }

  updateReportesEstatus(estatus, id) {
    return this.http.get(this.API_BASE_URL + 'ReportesService/updateReportesEstatus/' + estatus + '/' + id);
  }

  getRelCat(objDenuncia) {
    return this.http.get(this.API_BASE_URL + 'RelCatService/getRelCat');
  }

  insertRelReporte(objUsuario) {
    return this.http.post(this.API_BASE_URL + 'RelReporteService/insertRelReporte', objUsuario);
  }

  getReporteLastID() {
    return this.http.get(this.API_BASE_URL + 'ReportesService/getReporteLastID');
  }

  getDependencias() {
    return this.http.get(this.API_BASE_URL + 'DependenciaService/getDependencias');
  }

  getDependenciaReporte(idReporte) {
    return this.http.get(this.API_BASE_URL + 'DependenciaService/getDependenciaReporte/' + idReporte);
  }

  insertDependencia(objDependencia) {
    return this.http.post(this.API_BASE_URL + 'DependenciaService/insertDependencia', objDependencia);
  }

  getRelDepRep() {
    return this.http.get(this.API_BASE_URL + 'RelDepRepService/getRelDepRep');
  }

  insertRelDepRep(objRelDepRep) {
    return this.http.post(this.API_BASE_URL + 'RelDepRepService/insertRelDepRep', objRelDepRep);
  }

  getRelDepCat() {
    return this.http.get(this.API_BASE_URL + 'RelDepCatService/getRelDepCat');
  }

  insertRelDepCat(objRelDepCat) {
    return this.http.post(this.API_BASE_URL + 'RelDepCatService/insertRelDepCat', objRelDepCat);
  }

  /*sendEmailSoporte(objCorreoSoporte) {
      return this.http.post(this.API_BASE_URL + 'RegistroUsuarios/SendMailSoporte', objCorreoSoporte)
  }*/
}
