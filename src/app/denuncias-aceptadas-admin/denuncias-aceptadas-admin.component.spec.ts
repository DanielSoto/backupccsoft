import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DenunciasAceptadasAdminComponent } from './denuncias-aceptadas-admin.component';

describe('DenunciasAceptadasAdminComponent', () => {
  let component: DenunciasAceptadasAdminComponent;
  let fixture: ComponentFixture<DenunciasAceptadasAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DenunciasAceptadasAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenunciasAceptadasAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
