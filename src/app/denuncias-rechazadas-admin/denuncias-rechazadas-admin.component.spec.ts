import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DenunciasRechazadasAdminComponent } from './denuncias-rechazadas-admin.component';

describe('DenunciasRechazadasAdminComponent', () => {
  let component: DenunciasRechazadasAdminComponent;
  let fixture: ComponentFixture<DenunciasRechazadasAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DenunciasRechazadasAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenunciasRechazadasAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
