import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DenunciaRechazadaComponent } from './denuncia-rechazada.component';

describe('DenunciaRechazadaComponent', () => {
  let component: DenunciaRechazadaComponent;
  let fixture: ComponentFixture<DenunciaRechazadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DenunciaRechazadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenunciaRechazadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
