import { Component, OnInit } from '@angular/core';
import $ from 'jquery';
import { Router, ActivatedRoute } from '@angular/router';

declare var $:any;
@Component({
  selector: 'app-admin-cliente',
  templateUrl: './admin-cliente.component.html',
  styleUrls: ['./admin-cliente.component.css']
})
export class AdminClienteComponent implements OnInit {
  public usuario;
  constructor(private _router: Router, private _getRouter: ActivatedRoute) { }

  ngOnInit() {
    this.usuario = this._getRouter.snapshot.paramMap.get('user');
    console.log(this.usuario)

    $(document).ready(function () {
      $('#table_id').DataTable();
    });


    (function ($) {

    $(".sidebar-dropdown > a").click(function() {
  $(".sidebar-submenu").slideUp(200);
  if (
    $(this)
      .parent()
      .hasClass("active")
  ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .parent()
      .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .next(".sidebar-submenu")
      .slideDown(200);
    $(this)
      .parent()
      .addClass("active");
  }
});

$("#close-sidebar").click(function() {
  $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
  $(".page-wrapper").addClass("toggled");
});




});

$(document).ready(function () {
  $('#entradafilter').keyup(function () {
     var rex = new RegExp($(this).val(), 'i');
       $('.contenidobusqueda tr').hide();
       $('.contenidobusqueda tr').filter(function () {
           return rex.test($(this).text());
       }).show();

       })

});

(function () {
  $('#dtBasicExample').DataTable();
  $('.dataTables_length').addClass('bs-select');
  });


  }

}
