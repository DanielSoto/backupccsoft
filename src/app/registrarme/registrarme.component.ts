import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalIndexComponent } from '../modal-index/modal-index.component';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert2';
import { Rest } from 'src/providers/rest';

@Component({
  selector: 'app-registrarme',
  templateUrl: './registrarme.component.html',
  styleUrls: ['./registrarme.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class RegistrarmeComponent implements OnInit {

  public expRegularName = /^[A-Za-z\s]+$/
  public expRegularEmail = /^[^@]+@[^@]+\.[a-zA-Z]{2,}$/

  public objRegistro = {
    "nombres": null,
    "apellidos": null,
    "correo": null,
    "sexo": null,
    "usuario": null,
    "contrasena": null
  }
  public msjError;
  public lstRespuesta: any;
  public lstRespuesta2: any;
  public page = "denuncia";
  public msjVisibleApellido = false;
  public msjVisibleNombre = false;
  public msjErrorCorreo = false;
  public switchGenero: any;
  public emailRegistro;
  data: any;
  denuncia: any;
  confirmacion: any;

  constructor(public dialog: MatDialog, private http: HttpClient, private _rest: Rest) { }

  ngOnInit() {
    this.validacionAcesso();
    this.limpiarCampos();
    this.switchGenero = true;
  }

  getCorreoUsername() {
    this._rest.getRegistroUsuariosPorCorreo(this.objRegistro.correo).subscribe(success => {
      this.lstRespuesta = success;
      (this.lstRespuesta.data.length >= 1) ? this.emailRegistro = true : this.emailRegistro = false
    }, Error => {
      swal.fire('¡Advertencia!', 'Por favor, contacte al administrador.', 'error')
    })
  }

  getUsername() {
    if (this.validacionAcesso()) {
      this.getCorreoUsername();
      this._rest.getRegistroUsuariosPorUsername(this.objRegistro.usuario).subscribe(success => {
        this.lstRespuesta = success;
        if (this.lstRespuesta.data.length < 1 && this.emailRegistro == false) {
          this.insertarUsuarios()
        } else {
          swal.fire('¡Advertencia!', 'El nombre de usuario que intenta registrar o correo ya existe', 'error')
        }
      }, Error => {
        swal.fire('¡Advertencia!', 'Por favor, contacte al administrador.', 'error')
      })
    } else {
      swal.fire('¡Advertencia!', 'Posibles errores: Campos vacíos, contraseñas no coinciden', 'error')
    }
  }
 

  insertarUsuarios() {
    this._rest.insertDatosRegistro(this.objRegistro).subscribe(success => {
      this.limpiarCampos();
      swal.fire('¡Éxito!', 'Registro exitoso.', 'success')
    }, Error => {
      swal.fire('¡Advertencia!', 'Por favor, contacte al administrador.', 'error')
    })

  }

  validacionSexo() {
    (!this.switchGenero) ? this.objRegistro.sexo = "H" : this.objRegistro.sexo = "M"
  }

  validacionNombre() {
    (this.expRegularName.test(this.objRegistro.nombres)) ? this.msjVisibleNombre = false : this.msjVisibleNombre = true;
  }

  validacionApellido() {
    (this.expRegularName.test(this.objRegistro.apellidos)) ? this.msjVisibleApellido = false : this.msjVisibleApellido = true;
  }

  validacionCorreo() {
    (this.expRegularEmail.test(this.objRegistro.correo)) ? this.msjErrorCorreo = false : this.msjErrorCorreo = true;
  }

  validacionAcesso() {
    this.validacionSexo()
    if (this.objRegistro.nombres == "" || this.objRegistro.nombres == null || this.objRegistro.nombres == undefined || this.objRegistro.apellidos == "" || this.objRegistro.apellidos == null || this.objRegistro.apellidos == undefined || this.objRegistro.correo == "" || this.objRegistro.correo == null || this.objRegistro.correo == undefined || this.objRegistro.usuario == "" ||
      this.objRegistro.usuario == null || this.objRegistro.usuario == undefined || this.objRegistro.contrasena == "" || this.objRegistro.contrasena == null || this.objRegistro.contrasena == undefined || this.objRegistro.sexo == "" || this.objRegistro.sexo == null || this.objRegistro.sexo == undefined || this.confirmacion == "" || this.confirmacion == null || this.confirmacion == undefined ||
      this.objRegistro.contrasena != this.confirmacion || this.msjVisibleNombre == true || this.msjVisibleApellido == true || this.msjErrorCorreo == true) {
      return false;
    } else {
      return true;
    }
  }

  limpiarCampos() {
    this.objRegistro.nombres = "";
    this.objRegistro.apellidos = "";
    this.objRegistro.correo = "";
    this.objRegistro.usuario = "";
    this.objRegistro.contrasena = "";
    this.objRegistro.sexo = false;
    this.confirmacion = "";
  }
}

