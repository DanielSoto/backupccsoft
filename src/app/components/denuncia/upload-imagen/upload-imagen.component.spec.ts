import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadImagenComponent } from './upload-imagen.component';

describe('UploadImagenComponent', () => {
  let component: UploadImagenComponent;
  let fixture: ComponentFixture<UploadImagenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadImagenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadImagenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
