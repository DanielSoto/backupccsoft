import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {DenunciaModalComponent} from '../denuncia-modal/denuncia-modal.component';
import {UploadImagenComponent} from '../upload-imagen/upload-imagen.component';
import {HttpClient} from '@angular/common/http';
import swal from 'sweetalert2';
import {Router} from '@angular/router';
import {RouterModule, Routes} from '@angular/router';
import {Rest} from 'src/providers/rest';
import {CargaImagenesService} from '../../../services/carga-imagenes.service';
import {error} from 'util';

@Component({
  selector: 'app-denuncia',
  templateUrl: './denuncia.component.html',
  styleUrls: ['./denuncia.component.css']
})
export class DenunciaComponent implements OnInit {

  public objCategoria = {
    nombre: null
  };

  public objDenuncia = {
    folio: null,
    nombre: null,
    telefono: null,
    telefonoAlt: null,
    nombreDenunciado: null,
    calle: null,
    entreCalles: null,
    colonia: null,
    cp: null,
    status: null,
    descripcion: null,
    correo: null,
    imgUrl: null,
    fecha: null
  };
  public categoria: string;
  public lstWizard = [
    {
      icono: 'glyphicon glyphicon-user',
      activo: true,
      id: 'denuncia'
    },
    {
      icono: 'glyphicon glyphicon-picture',
      activo: false,
      id: 'evidencia'
    },
    {
      icono: 'glyphicon glyphicon-eye-open',
      activo: false,
      id: 'preview'
    }
    ,
    {
      icono: 'glyphicon glyphicon-ok',
      activo: false,
      id: 'CorrectoFRM'
    }
  ];
  mensaje = '';
  public objUsuario = {
    usuario: null
  };
  // variables para la subida de imagenes
  fileData: File = null;
  type: string;
  subir: boolean;
  previewUrl: string | ArrayBuffer;
  lastID = 0;
  lstRespuesta: any;
  public page = 'denuncia';
  denuncia: string;  // obj con el que puedes enviar información hacia el modal;

  constructor(public dialog: MatDialog, private http: HttpClient, private router: Router, private _rest: Rest, private _cargarIamgen: CargaImagenesService) {

  }

  public objDependencia = {
    nombre: null,
    municipio: null,
    calle: null,
    entreCalles: null,
    responsable: null,
    correo: null,
    telefono: null
  };

  ngOnInit() {
    this.limpiarCampos();
    this.getUser();
    this.subir = false;
  }

  getUser() {
    if (localStorage.getItem('usuario')) {
      this.objUsuario.usuario = localStorage.getItem('usuario');
    }
  }

  // Abrir checklist modal
  siguienteAnterio(id) {
    for (const index in this.lstWizard) {
      this.lstWizard[index].activo = false;
      if (id == this.lstWizard[index].id) {
        this.lstWizard[index].activo = true;
      }
    }
    this.page = id;
  }

  // lanza el dialog que envia o no el reporte
  openDialog(): void {
    if (this.categoriaSeleccionada()) {
      const dialogRef = this.dialog.open(DenunciaModalComponent, {
        width: '1000px',
        data: {name: this.objDenuncia.nombre}
      });
      dialogRef.afterClosed().subscribe(result => {
        this.insertarReporte(result);
      });
    }
  }

  // retorna false si no se seleccion? una categor?a y, en ese caso, regresa autom?ticamente al primer form.
  categoriaSeleccionada() {
    if (this.categoria == 'na') {
      swal.fire('¡Advertencia!', 'Debe seleccionar una categoría.', 'info');
      this.page = 'denuncia';
      return false;
    } else {
      return true;
    }
  }

  // inserta el reporte
  insertarReporte(result) {
    this.mensaje = '';
    if (result == '0') {
      this.validarNumeroVacio(0);
      this.getFecha();
      if (this.fileData != null) {
        this.cargarImagenes();
        console.log(this.objDenuncia.imgUrl);
      } else {
        this.insertDenuncia();
      }
    }
  }

  insertDenuncia() {
    this._rest.insertDenuncia(this.objDenuncia).subscribe(success => {
      this.insertarRelCat();
      if (this.objUsuario.usuario != null) {
        this.insertRelReporte();
      }
      swal.fire('¡Éxito!', 'Registro de denuncia exitoso.<br>Si ha cargado una imagen, espere la notificación de que fue cargada correctamente.', 'success');
    }, Error => {
      swal.fire('¡Advertencia!', 'No se ha podido registrar su denuncia', 'error');
    });
    this.validarNumeroVacio(1);
  }

  // inserta RelCat una vez insertado el reporte
  insertarRelCat() {
    this._rest.insertRelCat(this.objCategoria).subscribe(success => {

    }, Error => {
      swal.fire('¡Advertencia!', 'Ha ocurrido un error al seleccionar la categoría de la denuncia', 'error');
    });
  }

  insertRelReporte() {
    this._rest.insertRelReporte(this.objUsuario).subscribe(success => {

    }, Error => {
      swal.fire('¡Advertencia!', 'Ha ocurrido un error al conectar el reporte con tu usuario', 'error');
    });
  }

  // Convierte los valores de numero vacios en 0, para poder insertarlos en la BD.
  // Si el valor es 0 porque ya se inserto en la BD o porque ocurrio un error, por ejemplo, los valores se vuelven a convertir en ''
  // Recibe 0 para convertir de '' a 0 y cualquier otro valor para convertir de 0 a ''
  validarNumeroVacio(modo) {
    if (modo == 0) {
      if (this.objDenuncia.telefono == '') {
        this.objDenuncia.telefono = '0';
      }
      if (this.objDenuncia.telefonoAlt == '') {
        this.objDenuncia.telefonoAlt = '0';
      }
      if (this.objDenuncia.cp == '') {
        this.objDenuncia.cp = '0';
      }
    } else {
      if (this.objDenuncia.telefono == '0') {
        this.objDenuncia.telefono = '';
      }
      if (this.objDenuncia.telefonoAlt == '0') {
        this.objDenuncia.telefonoAlt = '';
      }
      if (this.objDenuncia.cp == '0') {
        this.objDenuncia.cp = '';
      }
    }
  }

  // llama las funciones de validacion y muestra un mensaje de error si alguna de ellas fallo
  validaciones() {
    let mensaje = '';
    mensaje = this.validacionCamposVacíos() + this.validacionLongitudes() + this.validacionCorreo() + this.validacionCategoria();
    if (mensaje == '') {
      this.openDialog();
    } else {
      swal.fire('¡Error!', mensaje, 'error');
      this.page = 'denuncia';
      return false;
    }

  }

  validacionCategoria() {
    this.mensaje = '';
    if (this.objCategoria.nombre == 'na') {
      return 'Debe seleccionar una categoría.<br>';
    }
    return this.mensaje;
  }

  // verifica las longitudes de CP y telefono
  validacionLongitudes() {
    console.log(this.objDenuncia);
    this.mensaje = '';
    if (this.objDenuncia.cp != '') {
      if (String(this.objDenuncia.cp).length != 5 && this.objDenuncia.cp != '') {
        this.mensaje += 'El código postal debe ser de 5 dígitos.<br>';
      }
    }
    if (this.objDenuncia.telefono != '' || this.objDenuncia.telefonoAlt != '') {
      if ((String(this.objDenuncia.telefono).length != 10 && this.objDenuncia.telefono != '') || (String(this.objDenuncia.telefonoAlt).length != 10 && this.objDenuncia.telefonoAlt != '')) {
        this.mensaje += 'Los números de teléfono deben ser de 10 dígitos.<br>';
        console.log(this.objDenuncia.telefono + ' ' + this.objDenuncia.telefonoAlt);
      }
    }
    return this.mensaje;

  }

  // valida que los campos obligatorios no esten vacios
  validacionCamposVacíos() {
    this.mensaje = '';
    if (this.objDenuncia.folio == '' ||
      this.objDenuncia.status == '' ||
      this.objDenuncia.descripcion == '' ||
      this.objCategoria.nombre == '') {
      return 'Asegúrese de haber ingresado una descripción para la denuncia.<br>';
    }
    return this.mensaje;
  }

  // valida el formato del correo
  validacionCorreo() {
    this.mensaje = '';
    if (this.objDenuncia.correo != '') {
      this.mensaje = '';
      const expRegular = /^[^@]+@[^@]+\.[a-zA-Z]{2,}$/;
      if (expRegular.test(this.objDenuncia.correo)) {
        return '';
      } else {
        return 'El formato del correo ingresado es incorrecto.<br>';
      }
    }
    return this.mensaje;
  }

  getFecha() {
    const time = new Date();
    const fecha = time.getFullYear() + '-' + (time.getMonth() + 1) + '-' + time.getDate() + ' ' + time.getHours() + ':' + time.getMinutes();
    this.objDenuncia.fecha = fecha;
  }

  limpiarCampos() {
    this.objDenuncia.folio = '0000000001';
    this.objDenuncia.nombre = '';
    this.objDenuncia.telefono = '';
    this.objDenuncia.telefonoAlt = '';
    this.objDenuncia.nombreDenunciado = '';
    this.objDenuncia.calle = '';
    this.objDenuncia.entreCalles = '';
    this.objDenuncia.colonia = '';
    this.objDenuncia.cp = '';
    this.objDenuncia.status = 'A';
    this.objDenuncia.descripcion = '';
    this.objDenuncia.correo = '';
    this.objDenuncia.imgUrl = 'N/A';
    this.objCategoria.nombre = 'na';
  }

// Subida de imagenes
  fileProgress(fileInput: any) {
    this.type = fileInput.target.files[0].type;
    if (this.type.includes('image')) {
      console.log('Iamgen');
      this.fileData = fileInput.target.files[0];
      this.subir = true;
      this.preview();
    } else {
      console.log('No imagen');
      this.subir = false;
    }
  }

  cargarImagenes() {
    let i = 0;
    this._rest.getReporteLastID().subscribe(success => {
        this.lstRespuesta = success;
        for (i = this.fileData.name.length; i > 0; i--) {
          if (this.fileData.name.charAt(i) == '.') {
            break;
          }
        }
        this.type = this.fileData.name.substring(i, this.fileData.name.length);
        const img = new File([this.fileData], (this.lstRespuesta.data[0].id_reportes + 1) + this.type, {type: this.fileData.type});
        this._cargarIamgen.cargarImagenesFirebase(img);
        this.objDenuncia.imgUrl = 'img/' + (this.lstRespuesta.data[0].id_reportes + 1) + this.type;
        this.insertDenuncia();
      },
      error => {
        swal.fire('Error', 'Error al subir la imgen.', 'error');
      });

  }

  preview() {
    // Show preview
    const mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    };
  }

  eliminarImg() {
    this.fileData = null;
    this.previewUrl = '';
  }

}
