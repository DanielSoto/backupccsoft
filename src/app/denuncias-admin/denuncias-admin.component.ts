import {Component, OnInit} from '@angular/core';
import $ from 'jquery';
import {Rest} from 'src/providers/rest';
import {Router} from '@angular/router';
import swal from 'sweetalert2';
import {delay} from 'rxjs/operators';

declare var $: any;

@Component({
  selector: 'app-misdenuncias',
  templateUrl: './denuncias-admin.component.html',
  styleUrls: ['./denuncias-admin.component.css']
})
export class DenunciasAdminComponent implements OnInit {

  constructor(private _rest: Rest, private _router: Router) {
  }

  admin = false;
  public lstRespuesta: any;
  public registros: 0;
  public estatus: any;
  public idStatus: any;
  public changeStatus: boolean;
  columnasTitle = ['Folio', 'Nombre', 'Tipo de denuncia', 'Atendido por', 'Fecha', 'PDF', 'Status'];
  public arregloDatos = [{
    folio: null,
    nombre: null,
    tipoDenuncia: null,
    atendido: null,
    fecha: null,
    pdf: null,
    status: null,
    clase: null
  }];
  public objUpdate = {
    estatus: null,
    id: null
  };
  reportes = [];

  usuario: string;
  status: string;

  onClick(event) {
    const target = event.target || event.srcElement || event.currentTarget;
    const idAttr = target.attributes.id;
    const value = idAttr.nodeValue;
    console.log(value);
  }

  openFile(event) {
    const target = event.target || event.srcElement || event.currentTarget;
    const idAttr = target.attributes.id;
    const value = idAttr.nodeValue;
    window.open(value);
  }

  getReportes() {
    let color = '';
    let pdfFile = '';
    let imgFile = '';
    this._rest.getReportesDenunciasAdmin().subscribe(success => {
        this.lstRespuesta = success;
        this.registros = this.lstRespuesta.data.length;
        console.log(this.registros);
        const output = this.lstRespuesta;
        for (let i = 0; i < this.registros; i++) {
          if (output.data[i].status == 'A') {
            this.status = 'RECIBIDA';
          } else if (output.data[i].status == 'B') {
            this.status = 'ACEPTADA';
          } else if (output.data[i].status == 'C') {
            this.status = 'RECHAZADA';
          }
          console.log(output.data[i].status);
          if (output.data[i].categoria == 'animal') {
            output.data[i].categoria = 'Maltrato animal';
          } else if (output.data[i].categoria == 'ambiental') {
            output.data[i].categoria = 'Daño ambiental';
          } else if (output.data[i].categoria == 'vialidad') {
            output.data[i].categoria = 'Daño en vialidad';
          } else if (output.data[i].categoria == 'dañoPublico') {
            output.data[i].categoria = 'Daño en instalación pública';
          }
          if (this.status == 'RECIBIDA') {
            color = 'btn btn-primary';
          } else if (this.status == 'ACEPTADA') {
            color = 'btn btn-success';
          } else if (this.status == 'RECHAZADA') {
            color = 'btn btn-danger';
          }

          pdfFile = 'https://firebasestorage.googleapis.com/v0/b/ccsoft-85144.appspot.com/o/pdf%2F' + output.data[i].id_reportes + '.pdf?alt=media';
          imgFile = 'https://firebasestorage.googleapis.com/v0/b/ccsoft-85144.appspot.com/o/img%2F' + output.data[i].id_reportes + '.jpg?alt=media';
          this.reportes.push({
            folio: output.data[i].id_reportes,
            nombre: output.data[i].nombre,
            tipo: output.data[i].categoria,
            descripcion: output.data[i].descripcion,
            atendido: 'Nombre de quien atienede',
            fecha: output.data[i].fecha,
            pdf: pdfFile,
            img: imgFile,
            status: this.status,
            clase: color,
          });
          this.estatus = this.status;
        }
        console.log(this.reportes);
      },
      Error => {
        console.log('Por favor, contacte al administrador.', 'error');
      });
  }

  getUser() {
    if (localStorage.getItem('usuario')) {
      this.usuario = localStorage.getItem('usuario');
    } else {
      this._router.navigateByUrl('login');
    }
  }

  update(event) {
    const target = event.target || event.srcElement || event.currentTarget;
    const idAttr = target.attributes.id;
    const value = idAttr.nodeValue;
    console.log(value);
    this.changeStatus = false;
    this.idStatus = value;
    this.objUpdate.id = value;
  }

  updateStatus(value) {
    this.changeStatus = true;
    this.objUpdate.estatus = value;
    console.log(this.objUpdate);
    this._rest.updateReportesEstatus(value, this.idStatus).subscribe(success => {
      swal.fire('Actualizado', 'El estatus se actualizó correctamente', 'success');
    }, error => {
      swal.fire('Error', 'El estatus no se pudo actualizar', 'error');
    });
    this.lstRespuesta = [];
    this.reportes = [];
    setTimeout(() => {
      this.getReportes();
    }, 1000);
  }

  ngOnInit() {
    this.getUser();
    this.getReportes();
    this.changeStatus = true;
    this.idStatus = 1;
  }

}
