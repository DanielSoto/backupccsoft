import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DenunciasAdminComponent } from './denuncias-admin.component';

describe('DenunciasAdminComponent', () => {
  let component: DenunciasAdminComponent;
  let fixture: ComponentFixture<DenunciasAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DenunciasAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenunciasAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
