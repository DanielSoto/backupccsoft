import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { IndexComponent } from '../index/index.component';
import { AppComponent } from '../app.component';

@NgModule({
  declarations: [
    AppComponent, 
    IndexComponent,
  ],
  imports: [
    CommonModule, BrowserModule, FormsModule, NgModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }