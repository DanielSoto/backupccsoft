import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {HttpClient} from '@angular/common/http';
import swal from 'sweetalert2';
import $ from 'jquery';
import {Rest} from 'src/providers/rest';
import {Router, ActivatedRoute} from '@angular/router';


declare var $: any;

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(public dialog: MatDialog, private http: HttpClient, private _rest: Rest, private _router: Router, private _getRouter: ActivatedRoute) {
  }

  data: any;
  tipoUsuario: any;
  usuario: any;
  public page = 'denuncia';
  public lstRespuesta: any;
  public msjError: any;
  public result: any;
  public objSesion = {
    usuario: null,
    contrasena: null
  };

  public objDependencia = {
    nombre: null,
    municipio: null,
    calle: null,
    entreCalles: null,
    responsable: null,
    correo: null,
    telefono: null
  };
  public objRelDepCat = {
    id_dependencia: null,
    id_categoria: null,

  };
  public objRelDepRep = {
    id_dependencia: null,
    id_reporte: null,
  };

  ngOnInit() {
    // this.logeado();


    this.objRelDepCat.id_dependencia = '1';
    this.objRelDepCat.id_categoria = '7';
    this.objRelDepRep.id_dependencia = '1';
    this.objRelDepRep.id_reporte = '107';
  }
  logeado() {
    if (localStorage.getItem('usuario')) {
      this._router.navigateByUrl('index');
    }
  }

  denunciaAnonima() {
    if (localStorage.getItem('usuario')) {
      localStorage.clear();
    }
    this._router.navigateByUrl('denuncia');
  }

  getUsuarioLogin() {
    if (this.validacionCamposVacios) {
      this._rest.getUserFromLogin(this.objSesion.usuario, this.objSesion.contrasena).subscribe(success => {
        this.lstRespuesta = success;
        (this.redireccionamiento(this.lstRespuesta));
      }, Error => {
        swal.fire('¡Advertencia!', 'Por favor, contacte al administrador.', 'error');
      });
    }
  }

  validacionCamposVacios() {
    if (this.objSesion.usuario == '' || this.objSesion.usuario == null || this.objSesion.usuario == undefined || this.objSesion.contrasena == '' || this.objSesion.contrasena == null || this.objSesion.contrasena == undefined) {
      return this.msjError = swal.fire('¡Advertencia!', 'Posibles errores: Campos vacíos', 'error');
    }
  }

  redireccionamiento(lstRespuesta) {
    if (lstRespuesta.data.length == 1) {
      localStorage.setItem('usuario', lstRespuesta.data[0].usuario);
      if (lstRespuesta.data[0].tipoUsuario == 'C') {
        this._router.navigateByUrl('index');
      } else if (lstRespuesta.data[0].tipoUsuario == 'A') {
        this._router.navigateByUrl('admin');
      }
      this.result = console.log(lstRespuesta.data[0].tipoUsuario);
    } else {
      this.result = swal.fire('¡Advertencia!', 'Posibles errores: Contraseña y/o usuario incorrecto', 'error');
    }
  }

  limpiarCampos() {
    this.objSesion.usuario = '';
    this.objSesion.contrasena = '';
  }

}
