import { Component, OnInit, ViewChild } from '@angular/core';
import $ from 'jquery';
import { MatDialog } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert2';
import { Rest } from 'src/providers/rest';
import { Router, ActivatedRoute } from '@angular/router';
import { _getOptionScrollPosition } from '@angular/material/core';

@Component({
  selector: 'app-miperfil',
  templateUrl: './miperfil.component.html',
  styleUrls: ['./miperfil.component.css']
})
export class MiperfilComponent implements OnInit {
  public objPerfil = {
    "id_registro":null,
    "usuario": null,
    "contrasena": null,
  }
  public nombre;
  public apellido;
  public user;
  public contrasenaConfirm;

  public lstRespuesta: any;

  public nombres;
  public apellidos;
  public usuario;
  public contrasena;
  public id_registro;

  data: any;

  constructor(public dialog: MatDialog, private http: HttpClient, private _rest: Rest, private _router: Router, private _getRouter: ActivatedRoute) { }

  ngOnInit() {
   this.getUser();
   this.getInfoPerfil();
  }

  getInfoPerfil() {
    debugger
    this._rest.getRegistroUsuariosPorUsername(this.user).subscribe(success => {
      this.lstRespuesta = success;
      this.nombre = this.lstRespuesta.data[0].nombres;
      this.apellido = this.lstRespuesta.data[0].apellidos;
      this.objPerfil.usuario = this.lstRespuesta.data[0].usuario;
      this.objPerfil.contrasena = this.lstRespuesta.data[0].contrasena;
      this.objPerfil.id_registro = this.lstRespuesta.data[0].id_registro
      this.contrasenaConfirm = this.objPerfil.contrasena;
    }, Error => {
      swal.fire('¡Advertencia!', 'Por favor, contacte al administrador.', 'error')
    })
  }

  updatePerfil() {
    if (!(this.objPerfil.usuario == "" || this.objPerfil.usuario == null || this.objPerfil.usuario == undefined || this.objPerfil.contrasena == "" || this.objPerfil.contrasena == null || this.objPerfil.contrasena == undefined
    || this.contrasenaConfirm == "" || this.contrasenaConfirm == null || this.contrasenaConfirm == undefined)) {
      if(this.objPerfil.contrasena == this.contrasenaConfirm){
      this._rest.updatePerfil(this.objPerfil).subscribe(success => {
        this.lstRespuesta = success;
        swal.fire('¡Exito!', 'Perfil actualizado correctamente. (Cuando inicia sesión nuevamente su nombre de usuario en el perfil cambiará)', 'success')
      }, Error => {
        swal.fire('¡Advertencia!', 'Por favor, contacte al administrador.', 'error')
      })
    }else{
      swal.fire('¡Advertencia!', 'Las contraseñas no coinciden.', 'error')
    }
    }else{
      swal.fire('¡Advertencia!', 'Por favor capture todos los campos solicitados.', 'error')
    }
  }

  getUser() {
    if (localStorage.getItem('usuario')) {
      this.user = localStorage.getItem('usuario');
    } else {
      this._router.navigateByUrl('login');
    }

  }
}
