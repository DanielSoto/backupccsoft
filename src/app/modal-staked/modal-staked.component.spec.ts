import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalStakedComponent } from './modal-staked.component';

describe('ModalStakedComponent', () => {
  let component: ModalStakedComponent;
  let fixture: ComponentFixture<ModalStakedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalStakedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalStakedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
