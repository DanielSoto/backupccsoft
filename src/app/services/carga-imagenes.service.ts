import {Injectable} from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import swal from 'sweetalert2';

@Injectable()
export class CargaImagenesService {

  private CARPETA_IMAGENES = 'img';

  constructor(private db: AngularFirestore) {
  }

  cargarImagenesFirebase(imagenes: File) {
    const storageRef = firebase.storage().ref();

    const uploadTask: firebase.storage.UploadTask = storageRef.child(`${this.CARPETA_IMAGENES}/${imagenes.name}`).put(imagenes);
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => console.log('subiendo'),
      (error) => console.error('Error al subir', error),
      () => {
        console.log('Imagen cargada correctamente.');
		swal.fire('¡Éxito!', 'Imagen cargada correctamente.', 'success');
        this.guardarImagen({
          nombre: imagenes.name,
        });
      });
  }

  private guardarImagen(imagen: any) {
    this.db.collection(`/${this.CARPETA_IMAGENES}`).add(imagen);
  }
}
