import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert2';
import $ from 'jquery';
import { Rest } from 'src/providers/rest';
import { Router } from '@angular/router';

@Component({
  selector: 'app-restaurar-pasword',
  templateUrl: './restaurar-pasword.component.html',
  styleUrls: ['./restaurar-pasword.component.css']
})

export class RestaurarPaswordComponent implements OnInit {
  public objCorreo = {
    "mensaje": null,
    "to": null,
    "subject": null
  }
  public visible;
  public visibleEmail;
  public visiblemsj;
  public visiblebutton1;
  public visiblebutton2;
  public ChangePassword;
  public visiblemsjIn;
  public emailRegistro;
  public lstRespuesta: any;
  public clave;
  data: any;

  constructor(public dialog: MatDialog, private http: HttpClient, private _rest: Rest, private _router: Router) { }

  ngOnInit() {
    this.visible = false;
    this.visiblemsj = false;
    this.visiblebutton2 = false;
    this.visibleEmail = true;
    this.visiblemsjIn = true;
    this.visiblebutton1 = true;
  }

  enviarcorreo() {
      this._rest.sendEmail(this.objCorreo).subscribe(success => {
        swal.fire('¡Exito!', 'Hemos enviado un correo para continuar con la recuperación de la contraseña', 'success')
        this.showInputs();
      }, Error => {
        swal.fire('¡Advertencia!', "Por favor, contacte al administrador.", 'error')
      })
  }

  getCorreoUsername() {
    if (!(this.objCorreo.to == "" || this.objCorreo.to == null || this.objCorreo.to == undefined)) {
      this._rest.getRegistroUsuariosPorCorreo(this.objCorreo.to).subscribe(success => {
        this.lstRespuesta = success;
        if (this.lstRespuesta.data.length >= 1){
          this.enviarcorreo();
        }else{
          swal.fire('¡Advertencia!', 'Lo sentimos, el correo electrónicos no se encontró en la base de datos.', 'error')
        }
      }, Error => {
        swal.fire('¡Advertencia!', 'Por favor, contacte al administrador.', 'error')
      })
    } else {
      swal.fire('¡Advertencia!', 'Por favor capture un correo electrónico', 'error')
    }
  }

  sendClave() {
    this._rest.getRegistroUsuariosPorClave(this.clave).subscribe(success => {
      this.lstRespuesta = success;
      console.log(this.lstRespuesta);
      if (this.lstRespuesta.data.length >= 1){
          this._router.navigate(["/crearcontraseña",this.clave]);
      }else{
        swal.fire('¡Advertencia!', "El código no coincide, vuelva a intentarlo.", 'error')
      }
      this.showInputs();
    }, Error => {
      swal.fire('¡Advertencia!', 'Por favor, contacte al administrador.', 'error')
    })
  }

  redirect() {
    this.visible = false;
    this.visiblemsj = false;
    this.visiblebutton2 = false;
    this.visibleEmail = true;
    this.visiblemsjIn = true;
    this.visiblebutton1 = true;
    this.objCorreo.to = "";
  }

  showInputs() {
    this.visible = true;
    this.visiblemsj = true;
    this.visiblebutton2 = true;
    this.visibleEmail = false;
    this.visiblemsjIn = false;
    this.visiblebutton1 = false;
  }

}




