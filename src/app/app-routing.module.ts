import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {RegistrarmeComponent} from './registrarme/registrarme.component';
import {SeguimientoComponent} from './seguimiento/seguimiento.component';
import {AdministradorComponent} from './administrador/administrador.component';
import {Page404Component} from './page404/page404.component';
import {DenunciaComponent} from './components/denuncia/denuncia-component/denuncia.component';
import {AdminClienteComponent} from './admin-cliente/admin-cliente.component';
import {FormularioAnonimoComponent} from './formulario-anonimo/formulario-anonimo.component';
import {PoliticasdeprivacidadComponent} from './politicasdeprivacidad/politicasdeprivacidad.component';
import {RestaurarPaswordComponent} from './restaurar-pasword/restaurar-pasword.component';
import {CrearpaswordComponent} from './crearpasword/crearpasword.component';
import {MisdependenciasComponent} from './misdependencias/misdependencias.component';
import {SoporteComponent} from './soporte/soporte.component';
import {MiperfilComponent} from './miperfil/miperfil.component';
import {AltDependenciasComponent} from './alt-dependencias/alt-dependencias.component';
import {MisdenunciasComponent} from './misdenuncias/misdenuncias.component';
import {DenunciaAceptadaComponent} from './denuncia-aceptada/denuncia-aceptada.component';
import {DenunciaRechazadaComponent} from './denuncia-rechazada/denuncia-rechazada.component';
import {DenunciasAdminComponent} from './denuncias-admin/denuncias-admin.component';
import {DenunciasAceptadasAdminComponent} from './denuncias-aceptadas-admin/denuncias-aceptadas-admin.component';
import {DenunciasRechazadasAdminComponent} from './denuncias-rechazadas-admin/denuncias-rechazadas-admin.component';


const routes: Routes = [
  {path: 'login', component: IndexComponent},
  {path: 'registro', component: RegistrarmeComponent},
  {path: 'seguimiento', component: SeguimientoComponent},
  {
    path: 'index', component: AdministradorComponent, children: [
      {path: 'misdenuncias', component: MisdenunciasComponent},
      {path: 'denuncia-aceptada', component: DenunciaAceptadaComponent},
      {path: 'denuncia-rechazada', component: DenunciaRechazadaComponent},
      {path: 'denuncia', component: DenunciaComponent},
      {path: 'miperfil', component: MiperfilComponent},
      {path: 'soporte', component: SoporteComponent},
    ]
  },
  {path: 'denuncia', component: DenunciaComponent},
  {
    path: 'admin', component: AdminClienteComponent, children: [
      {path: 'denunciasAdmin', component: DenunciasAdminComponent},
      {path: 'denuncia-aceptada', component: DenunciasAceptadasAdminComponent},
      {path: 'denuncia-rechazada', component: DenunciasRechazadasAdminComponent},
      {path: 'denuncia', component: DenunciaComponent},
      {path: 'miperfil', component: MiperfilComponent},
      {path: 'soporte', component: SoporteComponent},
    ]
  },
  {path: 'denunciaAnonima', component: FormularioAnonimoComponent},
  {path: 'PoliticasDePrivacidad', component: PoliticasdeprivacidadComponent},
  {path: 'restaurarContraseña', component: RestaurarPaswordComponent},
  {path: 'crearcontraseña/:clave', component: CrearpaswordComponent},
  {path: 'misdependencias', component: MisdependenciasComponent},
  {path: 'soporte', component: SoporteComponent},
  {path: 'miperfil', component: MiperfilComponent},
  {path: 'altadependencias', component: AltDependenciasComponent},
  {path: 'misdenuncias', component: MisdenunciasComponent},
  {path: 'denuncia-rechazada', component: DenunciaRechazadaComponent},
  {path: 'denuncia-aceptada', component: DenunciaAceptadaComponent},
  {path: '**', component: IndexComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
