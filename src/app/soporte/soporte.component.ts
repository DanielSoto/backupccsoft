import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert2';
import { Rest } from 'src/providers/rest';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-soporte',
  templateUrl: './soporte.component.html',
  styleUrls: ['./soporte.component.css']
})
export class SoporteComponent implements OnInit {
  public objSoporte = {
    "nombre": null,
    "correo": null,
    "celular": null,
    "asunto": null,
    "mensaje": null
  }
  public lstRespuesta: any;

  constructor(public dialog: MatDialog, private http: HttpClient, private _rest: Rest, private _router: Router, private _getRouter: ActivatedRoute) { }

  ngOnInit() {
  }
  clipboard() {
    let copy = "contactoccsoft@gmail.com";
    const selBox = document.createElement('textarea');
    selBox.value = copy;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    swal.fire('¡Listo!','Nuestro email se ha copiado a tu portapapeles','success');
  }
  /*sendMailSoporte(){
    debugger
    if(!(this.objSoporte.nombre=="" || this.objSoporte.nombre==null || this.objSoporte.nombre==undefined || this.objSoporte.correo=="" || this.objSoporte.correo==null || this.objSoporte.correo==undefined ||
    this.objSoporte.celular=="" || this.objSoporte.celular==null || this.objSoporte.celular==undefined || this.objSoporte.asunto=="" || this.objSoporte.asunto==null || this.objSoporte.asunto==undefined ||
    this.objSoporte.mensaje=="" || this.objSoporte.mensaje==null || this.objSoporte.mensaje==undefined)){
    this._rest.sendEmailSoporte(this.objSoporte).subscribe(success => {
      this.lstRespuesta = success;
      swal.fire('¡Exito!', "Correo enviado, nosotros nos contactaremos con usted", 'success')
      this.clearInput();
    }, Error => {
      swal.fire('¡Advertencia!', 'Por favor, contacte al administrador.', 'error')
    })
  }else{
    swal.fire('¡Advertencia!', 'Por favor capture todos los campos.', 'error')
  }
  }

  clearInput() {
    this.objSoporte.nombre="";
    this.objSoporte.correo="";
    this.objSoporte.celular="";
    this.objSoporte.asunto="";
    this.objSoporte.mensaje="";
  }*/

}
