import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DenunciaAceptadaComponent } from './denuncia-aceptada.component';

describe('DenunciaAceptadaComponent', () => {
  let component: DenunciaAceptadaComponent;
  let fixture: ComponentFixture<DenunciaAceptadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DenunciaAceptadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenunciaAceptadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
