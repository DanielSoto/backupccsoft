import { Component, OnInit } from '@angular/core';
import {Rest} from "../../providers/rest";
import {Router} from "@angular/router";

@Component({
  selector: 'app-denuncia-aceptada',
  templateUrl: './denuncia-aceptada.component.html',
  styleUrls: ['./denuncia-aceptada.component.css']
})
export class DenunciaAceptadaComponent implements OnInit {

  constructor(private _rest: Rest, private _router: Router) {
  }

  public lstRespuesta: any;
  public registros: 0;
  columnasTitle = ['Folio', 'Nombre', 'Tipo de denuncia', 'Atendido por', 'Fecha', 'PDF', 'Status'];
  public arregloDatos = [{
    folio: null,
    nombre: null,
    tipoDenuncia: null,
    atendido: null,
    fecha: null,
    pdf: null,
    status: null
  }]
  reportes = [];

  usuario: string;

  getReportes() {
    this._rest.getReportesDenuncias(this.usuario).subscribe(success => {
      this.lstRespuesta = success;
      this.registros = this.lstRespuesta.data.length;
      console.log(this.registros);
      let output = this.lstRespuesta;
      for (let i = 0; i < this.registros; i++) {
        if (output.data[i].status == 'B') {
          this.reportes.push({
            folio: output.data[i].id_reportes,
            nombre: output.data[i].nombre,
            tipo: output.data[i].categoria,
            descripcion: output.data[i].descripcion,
            atendido: 'Nombre de quien atienede',
            fecha: output.data[i].fecha,
            pdf: 'Link al pdf',
            status: 'ACEPTADA'
          });
        }
      }
      console.log(this.reportes);
    }, Error => {
      console.log('Por favor, contacte al administrador.', 'error')
    });
  }

  getUser() {
    if (localStorage.getItem('usuario')) {
      this.usuario = localStorage.getItem('usuario');
    } else {
      this._router.navigateByUrl('login');
    }
  }

  ngOnInit() {
    this.getUser();
    this.getReportes();
  }
}
