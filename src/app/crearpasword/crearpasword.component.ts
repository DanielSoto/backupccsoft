import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert2';
import $ from 'jquery';
import { Rest } from 'src/providers/rest';
import { Router, ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-crearpasword',
  templateUrl: './crearpasword.component.html',
  styleUrls: ['./crearpasword.component.css']
})
export class CrearpaswordComponent implements OnInit {
  public clave;
  public confirmPassword;
  public lstRespuesta: any;
  public objChangePassword = {
    "contrasena": null,
    "clave": null
  }
  data: any;

  constructor(public dialog: MatDialog, private http: HttpClient, private _rest: Rest, private _router: Router, private _getRouter: ActivatedRoute) { }

  ngOnInit() {
  }

  changePassword() {
    debugger
    if (!(this.objChangePassword.contrasena == null || this.objChangePassword.contrasena == "" || this.objChangePassword.contrasena == undefined) &&
      !(this.confirmPassword == null || this.confirmPassword == "" || this.confirmPassword == undefined)) {
      this.objChangePassword.clave = this._getRouter.snapshot.paramMap.get('clave');
      if (this.objChangePassword.contrasena == this.confirmPassword) {
        this._rest.updateContrasena(this.objChangePassword).subscribe(success => {
          this.lstRespuesta = success;
          swal.fire('¡Exito!', "Contraseña modificada correctamente", 'success')
          this.clearInput();
          this._router.navigate(["/login"]);
        }, Error => {
          swal.fire('¡Advertencia!', 'Por favor, contacte al administrador.', 'error')
        })
      } else {
        swal.fire('¡Advertencia!', 'Las contraseñas no coinciden.', 'error')
      }
    } else {
      swal.fire('¡Advertencia!', 'Por favor capture una contraseña en ambos campos.', 'error')
    }
  }

  clearInput() {
    this.objChangePassword.contrasena = "";
    this.confirmPassword = "";
  }
}
     //console.log (this._getRouter.snapshot.paramMap.get('clave'));
