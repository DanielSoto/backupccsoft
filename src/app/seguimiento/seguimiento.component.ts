import { Component, OnInit } from '@angular/core';
import $ from 'jquery';
declare var $:any;

@Component({
  selector: 'app-seguimiento',
  templateUrl: './seguimiento.component.html',
  styleUrls: ['./seguimiento.component.css']
})
export class SeguimientoComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    $(document).ready(function () {
      $('#table_id').DataTable();
    });
  }

}
