import {Component, OnInit} from '@angular/core';
import $ from 'jquery';
import {Router, ActivatedRoute} from '@angular/router';
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";

declare var $: any;


@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.css']
})
export class AdministradorComponent implements OnInit {
  public usuario;

  constructor(private _router: Router, private _getRouter: ActivatedRoute, private sanitizer: DomSanitizer) {
  }

  current_url: SafeUrl;

  cambiarUrl(url) {
    this.current_url = this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  ngOnInit() {
    if (localStorage.getItem('usuario')) {
      this.usuario = localStorage.getItem('usuario');
    } else {
      this._router.navigateByUrl('login');
    }
    this.current_url = this.sanitizer.bypassSecurityTrustResourceUrl('');
    (function ($) {

      $(".sidebar-dropdown > a").click(function () {
        $(".sidebar-submenu").slideUp(200);
        if (
          $(this)
            .parent()
            .hasClass("active")
        ) {
          $(".sidebar-dropdown").removeClass("active");
          $(this)
            .parent()
            .removeClass("active");
        } else {
          $(".sidebar-dropdown").removeClass("active");
          $(this)
            .next(".sidebar-submenu")
            .slideDown(200);
          $(this)
            .parent()
            .addClass("active");
        }
      });

      $("#close-sidebar").click(function () {
        $(".page-wrapper").removeClass("toggled");
      });
      $("#show-sidebar").click(function () {
        $(".page-wrapper").addClass("toggled");
      });


    });

    $(document).ready(function () {
      $('#entradafilter').keyup(function () {
        var rex = new RegExp($(this).val(), 'i');
        $('.contenidobusqueda tr').hide();
        $('.contenidobusqueda tr').filter(function () {
          return rex.test($(this).text());
        }).show();

      })

    });

    (function () {
      $('#dtBasicExample').DataTable();
      $('.dataTables_length').addClass('bs-select');
    });

    $(document).ready(function () {
      $('#table_id').DataTable();
    });
  }

  cerrarSesion() {
    localStorage.clear();
    this._router.navigateByUrl('login');
  }
}
