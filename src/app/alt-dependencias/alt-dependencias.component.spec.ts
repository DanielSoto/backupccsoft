import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltDependenciasComponent } from './alt-dependencias.component';

describe('AltDependenciasComponent', () => {
  let component: AltDependenciasComponent;
  let fixture: ComponentFixture<AltDependenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltDependenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltDependenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
