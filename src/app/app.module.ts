import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {IndexComponent} from './index/index.component';
import {DenunciaComponent} from './components/denuncia/denuncia-component/denuncia.component';
import {RegistrarmeComponent} from './registrarme/registrarme.component';
import {Page404Component} from './page404/page404.component';
import {SeguimientoComponent} from './seguimiento/seguimiento.component';
import {AdministradorComponent} from './administrador/administrador.component';
import {MatSliderModule} from '@angular/material/slider';
import {MatDialogModule} from '@angular/material/dialog';
import {DenunciaModalComponent} from './components/denuncia/denuncia-modal/denuncia-modal.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ModalStakedComponent} from './modal-staked/modal-staked.component';
import {ModalIndexComponent} from './modal-index/modal-index.component';
import {FormsModule} from '@angular/forms';
import {AdminClienteComponent} from './admin-cliente/admin-cliente.component';
import {FormularioAnonimoComponent} from './formulario-anonimo/formulario-anonimo.component';
import {ModalFormularioAnomComponent} from './modal-formulario-anom/modal-formulario-anom.component';
import {PoliticasdeprivacidadComponent} from './politicasdeprivacidad/politicasdeprivacidad.component';
import {HttpClientModule} from '@angular/common/http';
import {RestaurarPaswordComponent} from './restaurar-pasword/restaurar-pasword.component';
import {CrearpaswordComponent} from './crearpasword/crearpasword.component';
import {MisdependenciasComponent} from './misdependencias/misdependencias.component';
import {SoporteComponent} from './soporte/soporte.component';
import {MiperfilComponent} from './miperfil/miperfil.component';
import {Rest} from 'src/providers/rest';
import {AltDependenciasComponent} from './alt-dependencias/alt-dependencias.component';
import {MisdenunciasComponent} from './misdenuncias/misdenuncias.component';
import {UploadImagenComponent} from './components/denuncia/upload-imagen/upload-imagen.component';
import {DenunciaRechazadaComponent} from './denuncia-rechazada/denuncia-rechazada.component';
import {DenunciaAceptadaComponent} from './denuncia-aceptada/denuncia-aceptada.component';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {environment} from '../environments/environment';
import {CargaImagenesService} from './services/carga-imagenes.service';
import {DenunciasAdminComponent} from './denuncias-admin/denuncias-admin.component';
import { DenunciasRechazadasAdminComponent } from './denuncias-rechazadas-admin/denuncias-rechazadas-admin.component';
import {DenunciasAceptadasAdminComponent} from './denuncias-aceptadas-admin/denuncias-aceptadas-admin.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    DenunciaComponent,
    RegistrarmeComponent,
    Page404Component,
    SeguimientoComponent,
    AdministradorComponent,
    DenunciaModalComponent,
    ModalStakedComponent,
    ModalIndexComponent,
    AdminClienteComponent,
    FormularioAnonimoComponent,
    ModalFormularioAnomComponent,
    PoliticasdeprivacidadComponent,
    RestaurarPaswordComponent,
    CrearpaswordComponent,
    MisdependenciasComponent,
    SoporteComponent,
    MiperfilComponent,
    AltDependenciasComponent,
    MisdenunciasComponent,
    UploadImagenComponent,
    DenunciaRechazadaComponent,
    DenunciaAceptadaComponent,
    DenunciasAdminComponent,
    DenunciasAceptadasAdminComponent,
    DenunciasRechazadasAdminComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatSliderModule,
    MatDialogModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
  ],
  providers: [Rest, HttpClientModule, CargaImagenesService],
  entryComponents: [DenunciaModalComponent, ModalIndexComponent, ModalFormularioAnomComponent],  // AQUI SE AGG LOS MODALES PARA QUE PUEDAN FUNCIONAR
  bootstrap: [AppComponent]
})
export class AppModule {
}
