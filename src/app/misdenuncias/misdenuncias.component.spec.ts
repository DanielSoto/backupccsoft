import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisdenunciasComponent } from './misdenuncias.component';

describe('MisdenunciasComponent', () => {
  let component: MisdenunciasComponent;
  let fixture: ComponentFixture<MisdenunciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisdenunciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisdenunciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
