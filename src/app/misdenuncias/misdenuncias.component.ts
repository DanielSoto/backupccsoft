import {Component, OnInit} from '@angular/core';
import $ from 'jquery';
import {Rest} from 'src/providers/rest';
import {Router} from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-misdenuncias',
  templateUrl: './misdenuncias.component.html',
  styleUrls: ['./misdenuncias.component.css']
})
export class MisdenunciasComponent implements OnInit {

  constructor(private _rest: Rest, private _router: Router) {
  }




  admin = false;
  public lstRespuesta: any;
  public registros: 0;
  columnasTitle = ['Folio', 'Nombre', 'Tipo de denuncia', 'Atendido por', 'Fecha', 'PDF', 'Status'];
  public arregloDatos = [{
    folio: null,
    nombre: null,
    tipoDenuncia: null,
    atendido: null,
    fecha: null,
    pdf: null,
    status: null
  }];
  reportes = [];

  usuario: string;
  status: string;
  onClick(event) {
    const target = event.target || event.srcElement || event.currentTarget;
    const idAttr = target.attributes.id;
    const value = idAttr.nodeValue;
    console.log(value);
  }
  getReportes() {
    this._rest.getReportesDenuncias(this.usuario).subscribe(success => {
        this.lstRespuesta = success;
        this.registros = this.lstRespuesta.data.length;
        console.log(this.registros);
        let output = this.lstRespuesta;
        for (let i = 0; i < this.registros; i++) {
          if (output.data[i].status == 'A') {
            this.status = 'RECIBIDA';
          } else if (output.data[i].status == 'B') {
            this.status = 'ACEPTADA';
          } else if (output.data[i].status == 'C') {
            this.status = 'RECHAZADA';
          }
          console.log(output.data[i].status);
          this.reportes.push({
            folio: output.data[i].id_reportes,
            nombre: output.data[i].nombre,
            tipo: output.data[i].categoria,
            descripcion: output.data[i].descripcion,
            atendido: 'Nombre de quien atienede',
            fecha: output.data[i].fecha,
            pdf: 'Link al pdf',
            status: this.status
          });
        }
        console.log(this.reportes);
      },
      Error => {
        console.log('Por favor, contacte al administrador.', 'error');
      });
  }

  getUser() {
    if (localStorage.getItem('usuario')) {
      this.usuario = localStorage.getItem('usuario');
    } else {
      this._router.navigateByUrl('login');
    }
  }

  ngOnInit() {
    this.getUser();
    this.getReportes();
    (function($) {

      $('.sidebar-dropdown > a').click(function() {
        $('.sidebar-submenu').slideUp(200);
        if (
          $(this)
            .parent()
            .hasClass('active')
        ) {
          $('.sidebar-dropdown').removeClass('active');
          $(this)
            .parent()
            .removeClass('active');
        } else {
          $('.sidebar-dropdown').removeClass('active');
          $(this)
            .next('.sidebar-submenu')
            .slideDown(200);
          $(this)
            .parent()
            .addClass('active');
        }
      });

      $('#close-sidebar').click(function() {
        $('.page-wrapper').removeClass('toggled');
      });
      $('#show-sidebar').click(function() {
        $('.page-wrapper').addClass('toggled');
      });
    });

    $(document).ready(function() {
      $('#entradafilter').keyup(function() {
        var rex = new RegExp($(this).val(), 'i');
        $('.contenidobusqueda tr').hide();
        $('.contenidobusqueda tr').filter(function() {
          return rex.test($(this).text());
        }).show();

      });

    });

    (function() {
      $('#dtBasicExample').DataTable();
      $('.dataTables_length').addClass('bs-select');
    });

    $(document).ready(function() {
      $('#table_id').DataTable();
    });
  }

}
