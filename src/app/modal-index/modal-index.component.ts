import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-modal-index',
  templateUrl: './modal-index.component.html',
  styleUrls: ['./modal-index.component.css']
})
export class ModalIndexComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ModalIndexComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit() {
      console.log(this.data); // this.data es la info que llega al modal, que enviaste desde la función que lo invoca;
    }

    onNoClick(): void {
      this.dialogRef.close();
    }

}
